import 'package:flutter/material.dart';

class GeneralProvider with ChangeNotifier {

  bool isReversed = false;

 // List<String> _items = [];
  List<int> numbers = [];



  List<int> getList() {
    if(numbers.isEmpty){
      numbers = List<int>.generate(101, (index) => index);
    }
    return numbers;
  }

  void updateList() {
    numbers = numbers.reversed.toList();
    notifyListeners();
  }

}