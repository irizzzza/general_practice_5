import 'package:flutter/material.dart';
import 'canvas_practice_item.dart';

class CanvasPractice extends StatefulWidget {
  @override
  _CanvasPracticeState createState() => _CanvasPracticeState();
}

class _CanvasPracticeState extends State<CanvasPractice> with SingleTickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    Orientation orientation = MediaQuery.of(context).orientation;
    return Scaffold(
      body:  Center(
        child: CustomPaint(
          painter: CanvasPracticeItem(
              lineColor: Colors.amber,
              completeColor: Colors.blueAccent,
              completePercent: 30,
              width: 20.0
          ),
        ),
      ) ,
    );
  }
}
