import 'dart:math';

import 'package:flutter/material.dart';

class CanvasPracticeItem extends CustomPainter {

  Color lineColor;
  Color completeColor;
  double completePercent;
  double width;
  CanvasPracticeItem({this.lineColor,this.completeColor,this.completePercent,this.width});
  @override
  void paint(Canvas canvas, Size size) {

    Paint line = new Paint()
      ..color = lineColor
      ..strokeCap = StrokeCap.butt
      ..style = PaintingStyle.stroke
      ..strokeWidth = width;

    Paint complete5 = new Paint()
      ..color = Colors.cyan
      ..strokeCap = StrokeCap.butt
      ..style = PaintingStyle.stroke
      ..strokeWidth = width;


    Offset center  =  Offset(size.width/2, size.height/2);
    double radius  = 25;
    canvas.drawCircle(
        center,
        radius,
        line
    );

    canvas.drawArc(
      Rect.fromCircle(center: center,radius: radius),
      pi/0.5,
      1,
      false,
      complete5,
    );
  }
  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}