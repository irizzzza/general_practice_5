import 'package:flutter/material.dart';
import 'package:general_practice_5/providers/general_provider.dart';
import 'package:general_practice_5/widgets/canvas/canvas_practice.dart';
import 'package:provider/provider.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<GeneralProvider>(
      builder: (BuildContext ctx, genProvider, _){
        return Scaffold(
        body: SafeArea(
          child: Stack(
            children: <Widget>[
              CanvasPractice(),
              Column(
                children: <Widget>[
                  RaisedButton(
                    child: Text('Reverse'),
                    onPressed: genProvider.updateList,
                  ),
                  Expanded(
                    child: ListView.builder(
                        itemCount: genProvider.getList().length,
                        itemBuilder: (BuildContext ctx, int index){
                          return ListTile(
                            title: Text('Item ${genProvider.getList()[index]}'),
                          );
                        }),
                  ),
                ],
              ),
            ],
          ),
        ),
        );
      },
    );
  }
}
